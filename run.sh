#!/usr/bin/bash

if [[ "$1" == "" ]]; then
  echo "Server not defined";
  exit;
fi
java @user_jvm_args.txt -jar $1 nogui
